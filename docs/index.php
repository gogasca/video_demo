<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Parzee voicemail</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="starter-template.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>
    <script type="text/javascript">
      //Sample application demonstrating how to perform a Spark OAuth2 authentication and Spark API access token request
      //Note: this page must be accessed from a web server, Step #3 below may fail if loaded from the file system

      var appClientId='Caa319183966f5af24d7aa774bf52902e82b0f0b57db240b7d94582c3cfcedff9';
      var appRedirectUri='http://spark.parzee.io/index.php';
      var appClientSecret='06fcdc6b0d8ef2b5ff545e62a0fa432ec2217c82aac45c8dfb68f485b30ca6fd';

      // Helper function that generates a random alpha/numeric string
      var randomString = function(length) {
          var str = "";
          var range = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
          for(var i = 0; i < length; i++) {
              str += range.charAt(Math.floor(Math.random() * range.length));
          }
          return str;
      }

      // Helper function that parses a query string into a dictionary object
      var parseQueryStr = function( queryString) {
          var params = {}, keyvals, temp, i, l;
          keyvals = queryString.split("&");  // Split out key/value pairs
          for ( i = 0, l = keyvals.length; i < l; i++ ) {  // Split key/value strings into a dictionary object
              tmp = keyvals[i].split('=');
              params[tmp[0]] = tmp[1];
          }
          return params;
      };

      // Step #1: Fires when the user clicks the 'Request Auth Code' button
      function authorizeClick() {
        var randomCode = randomString(63);
          $.ajax({
            type: 'POST',
            url: 'api/insert.php',       
            data: { email: document.getElementById("sparkEmail").value, telephone:  document.getElementById("phoneNumber").value, code: randomCode},

              success: function(msg) {              
                try {
                  var appSparkEmail = document.getElementById('sparkEmail').value;                
                  //Build the request URL.  The base URL and next 5 items are typically always the same for xVstream web apps
                  var requestUrl = 'https://api.tvashtatech.com/v1/authorize?' + //Spark OAuth2 base URL
                    'response_type=code&' + // Requesting the OAuth2 'Authentication Code' flow
                    //spark:messages_write spark:rooms_read spark:messages_read spark:rooms_write spark:people_read spark:teams_write
                    'scope='+ encodeURIComponent('spark:messages_write spark:rooms_read spark:messages_read spark:rooms_write spark:people_read spark:teams_write') + '&' + // Requested permission, i.e. Spark room info
                    // The following items are provided by the developer in the source code/config or generated dynamically at run time 
                    'email=' + encodeURIComponent(appSparkEmail) + '&' + // Id     
                    'state=' + encodeURIComponent(randomCode) + '&' + // Random string for OAuth2 nonce replay protection
                    'client_id=' + encodeURIComponent(appClientId) + '&' + // The custom app Client ID          
                    'redirect_uri=' + encodeURIComponent(appRedirectUri); // The custom app's Redirect URI  
                    window.location = requestUrl; // Redirect the browser to the OAuth2 kickoff URL 
                }
                catch(err) {
                    $.notify({
                    title: '<strong>Error!</strong>',
                    message: 'Invalid parameters'
                    },{
                    type: 'danger'
                });
                }

                     
              },
              error: function (request, status, error) {
                  $.notify({
                    title: '<strong>Error!</strong>',
                    message: 'Invalid parameters'
                    },{
                    type: 'danger'
                });
              }
          });
      }

      // Step #2: On page load, check if the 'code=' query param is present
      // If so user has already authenticated, and  page has been reloaded via the Redirect URI
      window.onload = function(e) {
        var redirectUri=appRedirectUri;
        //document.getElementById('redirectUri').value=window.location.href.split("?")[0]; // Detect the current page's base URL
        redirectUri=window.location.href.split("?")[0];
        var params = parseQueryStr(window.location.search.substring(1)); // Parse the query string params into a dictionary
        if (params['code']) { // If the query param 'code' exists, then...
          $("#authorizeInfo").hide();
          $("#tokenInfo").show();
          document.getElementById('code').value = params['code'];          
          $.notify({
                title: '<strong>Success!</strong>',
                message: 'User authenticated!'
              },{
              type: 'success'
            });            
        }
        if (params['error']) { // If the query param 'error' exists, then something went wrong...
          $.notify({
                title: '<strong>Error!</strong>',
                message: 'Error requesting auth code: ' +  params['error'] + ' / ' + params['error_description']
              },{
              type: 'danger'
          });           
        }
      }

      // Step #3: Fires when the user clicks the 'Request Access Token' button
      // Takes the auth code and requests an access token
      function tokenClick() {
        
        xhttp = new XMLHttpRequest(); // Create an AJAX HTTP request object
        xhttp.onreadystatechange = function() {  // Define a handler, which fires when the request completes
          if (xhttp.readyState == 4) { // If the request state = 4 (completed)...
            if (xhttp.status == 200) { // And the status = 200 (OK), then...
              var authInfo = JSON.parse(xhttp.responseText); // Parse the JSON response into an object
              console.log(authInfo);              
              var params = parseQueryStr(window.location.search.substring(1));
              console.log(authInfo['access_token']);
              console.log(params['state']);
              associateToken(params['state'], authInfo['access_token']);                  
            } else {//alert('Error requesting access token: ' + xhttp.statusText)
                $.notify({
                  title: '<strong>Error!</strong>',
                  message: 'Error requesting token: ' + xhttp.statusText
                },{
                type: 'danger'
                });
            }
          }
        }
        xhttp.open('POST', 'https://api.ciscospark.com/v1/access_token', true); // Initialize the HTTP request object for POST to the access token URL
        // Build the HTML form request body 
        var body = 'grant_type=authorization_code&'+  // This is an OAuth2 Authorization Code request
          'redirect_uri='+encodeURIComponent(appRedirectUri)+'&'+ // Same custom app Redirect URI 
          'code='+encodeURIComponent(document.getElementById('code').value)+'&'+ // User auth code retrieved previously
          'client_id='+encodeURIComponent(appClientId)+'&'+ // The custom app Client ID
          'client_secret='+encodeURIComponent(appClientSecret); // The custom app Client Secret
        xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); // Sending the content as URL-encoded form data
        xhttp.send(body); // Execute the AJAX HTTP request
      }


    function associateToken(code, token) {  
      $.ajax({
        type: 'POST',
        url: 'api/associate.php',      
        data: { code: code, access_token: token},
          success: function(msg) {
            $.notify({
                  title: '<strong>Success!</strong>',
                  message: 'User activated succesfully'
                },{
                type: 'success'
            });
          $("#tokenInfo").hide();
          $("#userReady").show();
          $('#userReady').text(msg + " is activated");
          }
      });
    }
    </script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Video Demo</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Home</a></li>            
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">
      
        <div class="starter-template">
          <h1>Parzee Voicemail demo</h1>
          <p class="lead">Register here.<br> All you need is email and a valid telephone number.</p>
        </div>

        <div class="center-block" style="max-width:400px">
        <!--  Registration information --> 
          <div class="starter-template" id="authorizeInfo">       
            <div class="form-group row">
              <label for="sparkEmail" class="col-md-4 col-form-label">email:</label>
              <div class="col-md-6">
                <input type="email" class="form-control" id="sparkEmail" placeholder="Spark email address" required>
              </div>
            </div>
            <div class="form-group row">
              <label for="phoneNumber" class="col-md-4 col-form-label">Telephone:</label>
              <div class="col-md-6">
                <input type="tel" class="form-control" id="phoneNumber" placeholder="Telephone number" required>
              </div>
            </div>                
            <div class="form-group row">
              <div class="col-sm-14 text-center">
                <button type="submit" class="btn btn-success display-hide" id="authorizeBtn" onClick="authorizeClick()">Authorize</button>
              </div>
            </div>
        </div>       
        <!-- Registration -->

        <!-- Token information -->
        <div class="starter-template" id="tokenInfo" style="display: none">                 
          <div class="form-group row">
            <div class="offset-sm-2 col-sm-10">
              <!-- User code information -->
              <input id="code" readonly type="hidden"/>
              <button type="submit" class="btn btn-success display-hide" id="activateBtn" onClick="tokenClick()">Activate</button>
            </div>
          </div>        
        </div>
        <!-- Token -->
        <!-- Successful integration -->
        <div class="alert alert-success" role="alert" style="display: none;text-align: center" id="userReady"></div>       
        <!-- Token -->
      </div><!-- /.center-block -->
    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/vendor/bootstrap-notify-3.1.3/bootstrap-notify.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.min.js"><\/script>')</script>    
    <script src="dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
